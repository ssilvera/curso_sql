INSERT INTO sucursal (id_sucursal,nombre,direccion) VALUES (1,'Tincidunt LLP','228-5223 Ut, C.');
INSERT INTO sucursal (id_sucursal,nombre,direccion) VALUES (2,'Fringilla Company','Apartado núm.: 584, 8449 Eget Ctra.');
INSERT INTO sucursal (id_sucursal,nombre,direccion) VALUES (3,'A Facilisis Foundation','Apdo.:815-8363 Libero Av.');
INSERT INTO sucursal (id_sucursal,nombre,direccion) VALUES (4,'Donec Ltd','Apdo.:464-9154 Sed ');
INSERT INTO sucursal (id_sucursal,nombre,direccion) VALUES (5,'Auctor Odio A Corp.','Apdo.:875-9997 Phasellus ');
INSERT INTO sucursal (id_sucursal,nombre,direccion) VALUES (6,'Dignissim Limited','7916 Ultricies Avda.');
INSERT INTO sucursal (id_sucursal,nombre,direccion) VALUES (7,'Magna Malesuada Vel LLC','615-4706 Blandit Avda.');
INSERT INTO sucursal (id_sucursal,nombre,direccion) VALUES (8,'Ullamcorper LLC','Apdo.:572-566 Sem Ctra.');
INSERT INTO sucursal (id_sucursal,nombre,direccion) VALUES (9,'Sagittis Semper PC','Apdo.:211-3903 Odio Carretera');
INSERT INTO sucursal (id_sucursal,nombre,direccion) VALUES (10,'Metus Vitae Foundation','442-3208 Aliquet C.');

INSERT INTO verduras (id_verdura,nombre,precio, tipo_verdura) VALUES (1,'manzana',82, 'Fruta');
INSERT INTO verduras (id_verdura,nombre,precio, tipo_verdura) VALUES (2,'cebolla',82, 'Hortaliza');
INSERT INTO verduras (id_verdura,nombre,precio, tipo_verdura) VALUES (3,'papa',20, 'Hortaliza');
INSERT INTO verduras (id_verdura,nombre,precio, tipo_verdura) VALUES (4,'banana',40, 'Fruta');
INSERT INTO verduras (id_verdura,nombre,precio, tipo_verdura) VALUES (5,'tomate',25, 'Fruta');
INSERT INTO verduras (id_verdura,nombre,precio, tipo_verdura) VALUES (6,'morron',80, 'Verdura');
INSERT INTO verduras (id_verdura,nombre,precio, tipo_verdura) VALUES (7,'acelga',25, 'Planta');
INSERT INTO verduras (id_verdura,nombre,precio, tipo_verdura) VALUES (11,'lechuga',10, 'Planta');

INSERT INTO stock (id_stock,id_verduras,id_sucursal,cantidad) VALUES (1,9,8,410);
INSERT INTO stock (id_stock,id_verduras,id_sucursal,cantidad) VALUES (2,4,7,137);
INSERT INTO stock (id_stock,id_verduras,id_sucursal,cantidad) VALUES (3,10,10,86);
INSERT INTO stock (id_stock,id_verduras,id_sucursal,cantidad) VALUES (4,3,7,228);
INSERT INTO stock (id_stock,id_verduras,id_sucursal,cantidad) VALUES (5,2,1,455);
INSERT INTO stock (id_stock,id_verduras,id_sucursal,cantidad) VALUES (6,9,7,90);
INSERT INTO stock (id_stock,id_verduras,id_sucursal,cantidad) VALUES (7,7,8,66);
INSERT INTO stock (id_stock,id_verduras,id_sucursal,cantidad) VALUES (8,2,4,417);
INSERT INTO stock (id_stock,id_verduras,id_sucursal,cantidad) VALUES (9,9,9,72);
INSERT INTO stock (id_stock,id_verduras,id_sucursal,cantidad) VALUES (10,11,10,236);
INSERT INTO stock (id_stock,id_verduras,id_sucursal,cantidad) VALUES (11,7,8,262);
INSERT INTO stock (id_stock,id_verduras,id_sucursal,cantidad) VALUES (12,3,8,265);
INSERT INTO stock (id_stock,id_verduras,id_sucursal,cantidad) VALUES (13,2,2,213);
INSERT INTO stock (id_stock,id_verduras,id_sucursal,cantidad) VALUES (14,11,2,378);
INSERT INTO stock (id_stock,id_verduras,id_sucursal,cantidad) VALUES (15,5,5,420);

INSERT INTO tipo_empleado (id,nombre) VALUES (1,'Empleado');
INSERT INTO tipo_empleado (id,nombre) VALUES (2,'Jefe');
INSERT INTO tipo_empleado (id,nombre) VALUES (3,'Cadete');
INSERT INTO tipo_empleado (id,nombre) VALUES (4,'Jefe');
INSERT INTO tipo_empleado (id,nombre) VALUES (5,'Empleado');
INSERT INTO tipo_empleado (id,nombre) VALUES (6,'Empleado');
INSERT INTO tipo_empleado (id,nombre) VALUES (7,'Jefe');
INSERT INTO tipo_empleado (id,nombre) VALUES (8,'Empleado');
INSERT INTO tipo_empleado (id,nombre) VALUES (9,'Cadete');
INSERT INTO tipo_empleado (id,nombre) VALUES (10,'Cadete');

INSERT INTO empleados (id_empleado,nombre,telefono,id_sucursal,id_tipo_empleado) VALUES (1,'Sonya Blankenship','(05578) 0614352',3,7);
INSERT INTO empleados (id_empleado,nombre,telefono,id_sucursal,id_tipo_empleado) VALUES (2,'Faith Reese','(039177) 149971',10,7);
INSERT INTO empleados (id_empleado,nombre,telefono,id_sucursal,id_tipo_empleado) VALUES (3,'Cora Aguilar','(037785) 080697',4,9);
INSERT INTO empleados (id_empleado,nombre,telefono,id_sucursal,id_tipo_empleado) VALUES (4,'Dorothy Riggs','(06794) 7655120',2,4);
INSERT INTO empleados (id_empleado,nombre,telefono,id_sucursal,id_tipo_empleado) VALUES (5,'Chantale Blackburn','(012) 56664482',6,1);
INSERT INTO empleados (id_empleado,nombre,telefono,id_sucursal,id_tipo_empleado) VALUES (6,'Audrey Schroeder','(012) 65938174',2,4);
INSERT INTO empleados (id_empleado,nombre,telefono,id_sucursal,id_tipo_empleado) VALUES (7,'Astra Park','(07646) 3718811',7,2);
INSERT INTO empleados (id_empleado,nombre,telefono,id_sucursal,id_tipo_empleado) VALUES (8,'Paloma Steele','(024) 46632959',4,4);
INSERT INTO empleados (id_empleado,nombre,telefono,id_sucursal,id_tipo_empleado) VALUES (9,'Adara Shaffer','(0336) 72482153',4,9);
INSERT INTO empleados (id_empleado,nombre,telefono,id_sucursal,id_tipo_empleado) VALUES (10,'Pearl Brady','(07115) 4067669',10,6);
INSERT INTO empleados (id_empleado,nombre,telefono,id_sucursal,id_tipo_empleado) VALUES (11,'Mari Stone','(036371) 216681',4,2);
INSERT INTO empleados (id_empleado,nombre,telefono,id_sucursal,id_tipo_empleado) VALUES (12,'Jana Garza','(036363) 854292',9,2);
INSERT INTO empleados (id_empleado,nombre,telefono,id_sucursal,id_tipo_empleado) VALUES (13,'Kessie Mathis','(0814) 94210331',9,10);
INSERT INTO empleados (id_empleado,nombre,telefono,id_sucursal,id_tipo_empleado) VALUES (14,'Belle Roberts','(01888) 7770594',8,4);
INSERT INTO empleados (id_empleado,nombre,telefono,id_sucursal,id_tipo_empleado) VALUES (15,'Kai Stone','(0286) 42646942',6,9);
INSERT INTO empleados (id_empleado,nombre,telefono,id_sucursal,id_tipo_empleado) VALUES (16,'Farrah Mcclure','(037251) 414515',3,2);
INSERT INTO empleados (id_empleado,nombre,telefono,id_sucursal,id_tipo_empleado) VALUES (17,'Sara Chang','(033132) 603783',10,6);
INSERT INTO empleados (id_empleado,nombre,telefono,id_sucursal,id_tipo_empleado) VALUES (18,'Beatrice Wood','(0451) 03050734',2,10);
INSERT INTO empleados (id_empleado,nombre,telefono,id_sucursal,id_tipo_empleado) VALUES (19,'Tatiana Mckay','(0859) 00740850',3,6);
INSERT INTO empleados (id_empleado,nombre,telefono,id_sucursal,id_tipo_empleado) VALUES (20,'Colleen Oneill','(0027) 05055633',2,2);
