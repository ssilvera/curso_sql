-- Crear tablas de la base Verduleria

create table sucursal(
	id_sucursal	NUMBER(5) PRIMARY KEY,
	nombre varchar2(100) not null,
	direccion varchar2(100) not null
);

create table verduras(
	id_verdura number(5) PRIMARY KEY,
	nombre varchar2(100) not null,
	precio number(5) not null,
	tipo_verdura varchar2(100)
);

create table stock(
	id_stock number(5) PRIMARY KEY,
	id_verduras  number(5) not null,
	id_sucursal  number(5) not null,
	cantidad  number(5)
);

create table tipo_empleado(
	id number(5) PRIMARY KEY,
	nombre varchar2(100) not null
);

create table empleados(
	id_empleado number(5) PRIMARY KEY,
	nombre varchar2(100) not null,
	telefono varchar2(50) not null,
	id_sucursal number(5) not null,
	id_tipo_empleado number(5) not null
);